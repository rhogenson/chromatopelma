(define-library (csc list)
  (export
    all
    any
    filter
    foldl
    foldr
    intersperse
    map-maybe)
  (import (scheme base))
  (begin


    (define (any1 f l)
      (and (not (null? l))
           (or (f (car l))
               (any1 f (cdr l)))))


    (define (any f . ls)
      (and (not (any1 null? ls))
           (or (apply f (map car ls))
               (apply any f (map cdr ls)))))


    (define (all f . ls)
      (or (any null? ls)
          (and (apply f (map car ls))
               (apply all f (map cdr ls)))))


    (define (foldl f acc . ls)
      (if (any null? ls)
        acc
        (apply foldl f (apply f acc (map car ls)) (map cdr ls))))


    (define (foldr f acc . ls)
      (if (any null? ls)
        acc
        (apply f (append (map car ls) (list (apply foldr f acc (map cdr ls)))))))


    (define (map-maybe f l)
      (foldr
        (lambda (x acc)
          (define fx (f x))
          (if fx
            (cons fx acc)
            acc))
        '()
        l))


    (define (filter f l)
      (foldr
        (lambda (x acc)
          (if (f x)
            (cons x acc)
            acc))
        '()
        l))


    (define (intersperse sep l)
      (if (null? l)
        '()
        (cons (car l)
          (foldr
            (lambda (x acc)
              (cons sep (cons x acc)))
            '()
            (cdr l)))))))
